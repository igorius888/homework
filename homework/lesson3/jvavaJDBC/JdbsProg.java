package jvavaJDBC;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbsProg {

	void base(String userName, String password) throws IOException {
		try {
			String adress;
			
			BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
			BufferedWriter write = new BufferedWriter(new OutputStreamWriter(System.out));
			
						
			Class.forName("com.mysql.jdbc.Driver");
		  //	String url = "jdbc:mysql://localhost/mydb";
			System.out.print("������� URL  ");
			String  url = read.readLine();
	
			// ������������ �������

			Connection connection = DriverManager.getConnection(url, userName,
					password);

			Statement statement = connection.createStatement();
			// �������� �������� �����
			
			
		    //	statement.addBatch("INSERT INTO user (id, name) VALUES (1234, 'newName22');");
			//	statement.addBatch("DELETE FROM user  WHERE  id=4");
			//	statement.addBatch("UPDATE user SET id=888 WHERE id=1234 ");
			System.out.print("������� ������   ");
			String  request = read.readLine();
			statement.addBatch(request);
			

			
			
			
			int[] results = statement.executeBatch();
			// ������ ����� � ���� ���� � ���������� �� ���������

			String sql = "SELECT * FROM user";
			ResultSet result = statement.executeQuery(sql);

			// ��� ��������, ��������� �� �����
			while (result.next()) {
				System.out.println("User name:  " + result.getString("name")
						+ "  ID:  " + result.getInt("id"));
			}
			connection.close();

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		JdbsProg jdbsProg = new JdbsProg();
		try {
			jdbsProg.base("root", "root");
		} catch (IOException e) {
			
			e.printStackTrace();
		}

	}
}
