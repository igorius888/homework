package javaSimple;

public class FractionSequence {
	void definition(int n) {

		for (int i = 1; i < n; i++) {
			System.out.println(1.0/i);
		}
	}

	public static void main(String[] args) {
		FractionSequence fracSeq = new FractionSequence();
		fracSeq.definition(10);
	}

}
