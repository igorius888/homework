package javaThread;
import java.io.File;
import java.io.FilenameFilter;

class Finder implements FilenameFilter {
	 String ext;
	public Finder(String ext){this.ext="."+ext;}
	
	
	public boolean accept(File dir, String name) {
		return name.endsWith(ext);}
}

