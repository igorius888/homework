package javaCollection;

import java.util.EmptyStackException;
import java.util.Stack;

public class Excepting {

	void divide(int x, int y) throws ArithmeticException {
		try {
			int z = x / y;
			System.out.println("Result of divide  " + z);

		} catch (ArithmeticException ae) {
			System.out.println("������� ���������� � �����" + ae);
		}
	}

	// ��������� ��`���(��������� ����� �����) ��� ����� �������� "null"
	public void adding(Object obj) {
		try {
			Stack stack = new Stack();
			stack.push(obj);
			if (stack.peek() == null)
				throw new NullPointerException();
		} catch (NullPointerException ne) {
			System.out.println(ne + "  Trouble! Stack is empty");
		}
	}

	public static void main(String[] args) {
		Excepting ex = new Excepting();
		ex.divide(5, 2); // set two numbers for divide
		ex.adding(4);// Add "null" or "some object(for simple: some  number)"

	}

}
