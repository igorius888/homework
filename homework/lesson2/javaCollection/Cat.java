package javaCollection;

import java.util.Arrays;

public class Cat {

int[] rgbColor; 
	
int age;


public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + age;
	result = prime * result + Arrays.hashCode(rgbColor);
	return result;
}


public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Cat other = (Cat) obj;
	if (age != other.age)
		return false;
	if (!Arrays.equals(rgbColor, other.rgbColor))
		return false;
	return true;
}


}
