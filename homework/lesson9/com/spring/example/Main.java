package com.spring.example;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext(
				"config.xml");
		Vehicle bmw = (Vehicle) ac.getBean("vehicle");
		System.out.println("controlPanel: " + bmw.getControlPanel());
	}

}
