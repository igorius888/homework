package com.spring.example;

public class ControlPanel {


	private Accelerator accelerator;
	private BrakePedal brakePedal;
	private Engine engine;
	private GasTank gasTank;
	private HardBrake hardBrake;
	private Horn horn;
	private SteeringWheel steeringWheel;
	private Forward forward;
	private Rear rear;
	
	public  ControlPanel(){}
	
	public Accelerator getAccelerator() {
		return accelerator;
	}
	public void setAccelerator(Accelerator accelerator) {
		this.accelerator = accelerator;
	}
	public BrakePedal getBrakePedal() {
		return brakePedal;
	}
	public void setBrakePedal(BrakePedal brakePedal) {
		this.brakePedal = brakePedal;
	}
	public Engine getEngine() {
		return engine;
	}
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public GasTank getGasTank() {
		return gasTank;
	}
	public void setGasTank(GasTank gasTank) {
		this.gasTank = gasTank;
	}
	public HardBrake getHardBrake() {
		return hardBrake;
	}
	public void setHardBrake(HardBrake hardBrake) {
		this.hardBrake = hardBrake;
	}
	public Horn getHorn() {
		return horn;
	}
	public void setHorn(Horn horn) {
		this.horn = horn;
	}
	public SteeringWheel getSteeringWheel() {
		return steeringWheel;
	}
	public void setSteeringWheel(SteeringWheel steeringWheel) {
		this.steeringWheel = steeringWheel;
	}
	public Forward getForward() {
		return forward;
	}
	public void setForward(Forward forward) {
		this.forward = forward;
	}
	public Rear getRear() {
		return rear;
	}
	public void setRear(Rear rear) {
		this.rear = rear;
	}
	
	
	

}
