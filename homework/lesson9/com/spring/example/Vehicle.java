package com.spring.example;

public class Vehicle {
	private ControlPanel controlPanel;

	public Vehicle() {
	}

	public ControlPanel getControlPanel() {
		return controlPanel;
	}

	public void setControlPanel(ControlPanel controlPanel) {
		this.controlPanel = controlPanel;
	}

}
