package comServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Form2Servlet
 */
@WebServlet("/Form2Servlet")
public class Form2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		ServletContext context = request.getServletContext();

		PrintWriter pw = response.getWriter();
/* next if(whereToStore){session} else {servletContext} */
	//	To store session
		if (session.getAttribute("whereToStore").equals("session")) {
			pw.println("whereToStore : session");
			session.setAttribute("name", "value");
			// ///////
			Object messageValue = session.getAttribute("message");
			pw.print("message=");
			pw.print(messageValue);
			pw.print(", ");

			Object namedValue = session.getAttribute("named");
			pw.print("named=");
			pw.print(namedValue);
			pw.print(", ");

			Object valuedValue = session.getAttribute("valued");
			pw.print("valued=");
			pw.print(valuedValue);
			pw.print(", ");

/* Action */
			String actionValue = (String) session.getAttribute("action");// action
			pw.print(actionValue + " = ");

			if (actionValue.equals("remove")) {
				session.removeAttribute("named");
				session.removeAttribute("message");
				session.removeAttribute("valued");
			} else if (actionValue.equals("add")) {
				session.setAttribute("message", messageValue);
				session.setAttribute("named", namedValue);
				session.setAttribute("valued", valuedValue);

			} else {
				session.getAttribute("message");
				session.getAttribute("named");
				session.getAttribute("valued");
			}
/* end Action */

		} else {
//	To store context
			pw.println("whereToStore : servletContex");
			context.setAttribute("name", "value");

			Object messageValue = context.getAttribute("message");
			pw.print("message=");
			pw.print(messageValue);
			pw.print(", ");

			Object namedValue = context.getAttribute("named");
			pw.print("named=");
			pw.print(namedValue);
			pw.print(", ");

			Object valuedValue = context.getAttribute("valued");
			pw.print("valued=");
			pw.print(valuedValue);
			pw.print(", ");
/* Action */
			String actionValue = (String) context.getAttribute("action");// action
			pw.print(actionValue + " = ");

			if (actionValue.equals("remove")) {
				context.removeAttribute("named");
				context.removeAttribute("message");
				context.removeAttribute("valued");
			} else if (actionValue.equals("add")) {
				context.setAttribute("message", messageValue);
				context.setAttribute("named", namedValue);
				context.setAttribute("valued", valuedValue);

			} else {
				context.getAttribute("message");
				context.getAttribute("named");
				context.getAttribute("valued");
			}
/* end Action */
			pw.close();
		}
	}

}
